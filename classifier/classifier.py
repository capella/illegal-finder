import cherrypy
import glob
import pickle
import os
from bs4 import BeautifulSoup
import urllib.request 
import re
import requests
import threading
import psycopg2
import gc

import traceback
import logging

# INSERT INTO status (id, description) VALUES
# (1, 'Aguardando'),
# (2, 'Processamento'),
# (3, 'Processado'),
# (4, 'Respondido');

conn_string = "host='db' dbname='postgres' user='postgres' password='example'"
filehandler = open("./result/result.pkl", 'rb')
tobin, vectorizer, classifiers = pickle.load(filehandler)

class classifierThread (threading.Thread):
    def __init__(self, sites, callback):
        threading.Thread.__init__(self)

        self.sites = sites
        self.callback = callback

    def site_request (self, url):
        headers = {
            "Connection": "keep-alive",
            "Cache-Control": "max-age=0",
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Language": "en-US,en;q=0.9"
        }
        try:
            request = urllib.request.Request(url, headers=headers)
            req = urllib.request.urlopen(request)
            encoding=req.headers['content-type'].split('charset=')[-1]
            encoding=encoding.split('Charset=')[-1]
            if encoding == "text/html":
                encoding = 'utf-8'
            return req.read().decode(encoding)
        except (Exception) as e:
            print("10", e)
        return None

    def extract_text (self, html):
        soup = BeautifulSoup(html, 'html.parser')
        [soup.extract() for soup in soup(['style', 'script', '[document]', 'head', 'title'])]
        content = " ".join(soup.strings)
        content = re.sub("\s\s+" , " ", content)
        return content

    def set_status (self, id_aux, status, conn):
        cursor = conn.cursor()
        sql = "INSERT INTO request_status (request_id, status_id) VALUES (%s, %s);"
        cursor.execute(sql, (id_aux, status))
        cursor.close()
        conn.commit()


    def get_classesname (self, conn):
        cursor = conn.cursor()
        sql = "SELECT id, name FROM illegalproduct;"
        cursor.execute(sql)
        rows = cursor.fetchall()
        cursor.close()
        return rows

    def run(self):

        try:

            conn = psycopg2.connect(conn_string)
            sites = self.sites
            callback = self.callback
            ids = []

            classes = {}
            aux = self.get_classesname(conn)
            for i in aux:
                classes[int(i[0])] = i[1]

            for site in sites:
                sql = "INSERT INTO request (url, callback) VALUES (%s, %s) RETURNING id;"
                cursor = conn.cursor()
                cursor.execute(sql, (site, callback))
                id_aux = cursor.fetchone()[0]
                cursor.close()
                conn.commit()
                ids.append(id_aux)
                self.set_status(id_aux, 1, conn) # Aguardando
                
            res = []
            errors = []
            reasons = []
            for j in range(len(sites)):
                site = sites[j]
                id_aux = ids[j]
                print(site, callback)

                self.set_status(id_aux, 2, conn) # Processando
                html = self.site_request(site)

                if html == None:
                    errors.append(site)
                    continue

                content = self.extract_text(html)
                content = vectorizer.transform([content])

                result = {}

                restrict = False
                for i in classifiers:
                    clf = i["c"]
                    prediction = clf.predict_proba(content)

                    c_id = str(i["id"])
                    result[c_id] = prediction[0][1]
                    if prediction[0][1] > 0.15:
                        restrict = True
                        reasons.append("Se parece com sites que vendem %s."%(classes[i["id"]]))

                res.append({
                    "url": site,
                    "reasons": reasons,
                    "result": result,
                    "restrict": restrict
                })
                self.set_status(id_aux, 3, conn) # Processado

            if len(errors) != 0:
                requests.post(callback, json={"error": "Something is wrong with your sites.", "erros": errors})
            else:
                requests.post(callback, json={"sites": res, "categories": classes})

            for i in ids:
                self.set_status(id_aux, 4, conn) # Respondido

            if conn is not None:
                conn.close()
                del conn

        except Exception as e:
            logging.error(traceback.format_exc())

        exit()

@cherrypy.tools.json_out()
def error_page_404(status, message, traceback, version):
    return "page not found"

@cherrypy.tools.json_out()
def error_page_400(status, message, traceback, version):
    return "invalid json"

class Root(object):
    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def index(self):
        data = cherrypy.request.json

        if "sites" not in data and not type(data["sites"]) is list:
            return "send a list of sites"

        if "callback" not in data and not type(data["callback"]) is str:
            return "specify a callback"

        thread = classifierThread(data["sites"], data["callback"])
        thread.setDaemon(True)
        thread.start()

        return "ok"

if __name__ == '__main__':

    global_conf = {
        'global': {
            'server.environment': 'production',
            'engine.autoreload_on': True,
            'engine.autoreload_frequency': 5,
            'server.socket_host': '0.0.0.0',
            'server.socket_port': 8000,
        }
    }

    cherrypy.tree.mount(Root(), '/')
    cherrypy.config.update(global_conf)
    cherrypy.config.update({'error_page.404': error_page_404})
    cherrypy.config.update({'error_page.500': error_page_404})
    cherrypy.config.update({'error_page.400': error_page_400})
    cherrypy.engine.start()


