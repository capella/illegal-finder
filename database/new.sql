-- Crawler
CREATE TABLE IF NOT EXISTS mainsite (
    id SERIAL PRIMARY KEY,
    base_url varchar(1023) NOT NULL UNIQUE,
    search boolean NOT NULL DEFAULT true
);

CREATE TABLE IF NOT EXISTS url (
    id SERIAL PRIMARY KEY,
    url varchar(1023) NOT NULL,
    mainsite_id int,
    CONSTRAINT unique_url UNIQUE (mainsite_id, url),
    FOREIGN KEY (mainsite_id) REFERENCES mainsite(id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS visit (
    id SERIAL PRIMARY KEY,
    html TEXT,
    content TEXT,
    hash varchar(256) NOT NULL,
    url_id int,
    created_at TIMESTAMP DEFAULT now(),
    CONSTRAINT unique_visit UNIQUE (created_at, url_id),
    CONSTRAINT unique_content UNIQUE (hash, url_id),
    FOREIGN KEY (url_id) REFERENCES url(id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS illegalproduct (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS illegal_mainsite (
    id SERIAL PRIMARY KEY,
    mainsite_id int,
    illegalproduct int,
    CONSTRAINT unique_relation UNIQUE (mainsite_id, illegalproduct),
    FOREIGN KEY (mainsite_id) REFERENCES mainsite(id)
        ON DELETE CASCADE,
    FOREIGN KEY (illegalproduct) REFERENCES illegalproduct(id)
);

-- Request
CREATE TABLE IF NOT EXISTS request (
    id SERIAL PRIMARY KEY,
    url varchar(1023),
    callback varchar(1023)
);

CREATE TABLE IF NOT EXISTS status (
    id SERIAL PRIMARY KEY,
    description varchar(1023)
);

CREATE TABLE IF NOT EXISTS request_status (
    id SERIAL PRIMARY KEY,
    request_id int,
    status_id int,
    created_at TIMESTAMP DEFAULT now(),
    FOREIGN KEY (request_id) REFERENCES request(id),
    FOREIGN KEY (status_id) REFERENCES status(id)
);

-- User
CREATE TABLE IF NOT EXISTS users (
    id SERIAL PRIMARY KEY,
    email varchar(255),
    password_hash varchar(255)
);

-- Insert user admin, pass admin
INSERT INTO users (email, password_hash)
VALUES ('admin', 'jGl25bVBBBW96Qi9Te4V37Fnqchz/Eu4qB9vKrRIqRg=');

-- Insert Illegal products Categories
INSERT INTO illegalproduct (name) VALUES
('Prostituição'),
('Medicamentos Proibidos'),
('Drogas Sintéticas'),
('Drogas Naturais'),
('Produtos Tecnológicos Ilícitos'),
('Cigarros'),
('Esquemas de Pirâmide'),
('Armas');

-- Insert status
INSERT INTO status (id, description) VALUES
(1, 'Aguardando'),
(2, 'Processamento'),
(3, 'Processado'),
(4, 'Respondido');

-- Insert Sites
INSERT INTO status (id, mainsite) VALUES
(1, g1.globo.com),
(2, pt.wikipedia.org),
(3, www.brasiltatica.com.br),
(4, www.estadao.com.br),
(5, www.falconarmas.com.br),
(6, www.linkrosa.com.br),
(7, www.queenflavor.com),
(8, www.travesticomlocal.com.br),
(9, www.vaporesabor.com.br);
