import urllib.request 
from bs4 import BeautifulSoup
import os, psycopg2, hashlib
from urllib.parse import urljoin
import urllib.parse
import re
import signal
import sys

from time import sleep

conn_string = "host='db' dbname='postgres' user='postgres' password='"+os.environ['POSTGRES_PASSWORD']+"'"
conn = None

def insert_visit (html, content, url_id):
    """ query parts from the parts table """
    sql = """
        INSERT INTO visit (html, content, url_id, hash)
        VALUES (%s, %s, %s, %s)
        ON CONFLICT ON CONSTRAINT unique_content DO NOTHING;
    """
    data_hash = hashlib.sha224(content.encode()).hexdigest()

    try:
        cur = conn.cursor()
        cur.execute(sql, (html, content, url_id, data_hash))
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

def insert_link (domain, url):

    # check if domain exist, insert if not
    # check if url exist, insert if not

    sql1 = """
        INSERT INTO mainsite (base_url, search)
        VALUES (%s, false)
        ON CONFLICT (base_url) DO UPDATE set id = mainsite.id
        RETURNING id
    """

    sql2 = """
        INSERT INTO url (mainsite_id, url)
        VALUES (%s, %s)
        ON CONFLICT ON CONSTRAINT unique_url DO UPDATE set id = url.id
        RETURNING id
    """

    try:
        cur = conn.cursor()
        cur.execute(sql1, (domain, ))
        domain_id = cur.fetchone()[0]
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        return None

    try:
        cur = conn.cursor()
        cur.execute(sql2, (domain_id, url, ))
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

def site_request (url):
    headers = {
        "Connection": "keep-alive",
        "Cache-Control": "max-age=0",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.9"
    }
    try:
        request = urllib.request.Request(url, headers=headers)
        req = urllib.request.urlopen(request)
        encoding=req.headers['content-type'].split('charset=')[-1]
        encoding=encoding.split('Charset=')[-1]
        if encoding == "text/html":
            encoding = 'utf-8'
        return req.read().decode(encoding)
    except (Exception) as e:
        print("10", e)
    return None

def get_and_insert_site (domain, url, url_id):
    site = domain+url

    if domain in url:
        site = url 

    if (".jpg" in url) or (".jpeg" in url) or ".png" in url:
        return

    final_site = "http://"+site
    html = site_request("http://"+site)
    if html == None:
        final_site = "https://"+site
        html = site_request("https://"+site)
    if html == None:

        sql = """
        DELETE FROM url WHERE url.id = %s
        """
        try:
            cur = conn.cursor()
            cur.execute(sql, (url_id, ))
            rows = cur.fetchall()
            cur.close()
            if (len(rows) == 1):
                return rows[0];
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        return

    soup = BeautifulSoup(html, 'html.parser')

    [soup.extract() for soup in soup(['style', 'script', '[document]', 'head', 'title'])]
    content = " ".join(soup.strings)
    content = re.sub("\s\s+" , " ", content)

    insert_visit("", content, url_id)

    links = []
    for link in soup.find_all('a'):
        url_link = urljoin(final_site, link.get('href'))

        if url_link == None:
            continue

        if url_link.startswith('http'):
            # host = urllib.parse.urlparse(url_link).netloc
            # path = url_link[url_link.index(host)+len(host):]
            host = urllib.parse.urlparse(url_link).netloc
            path = urllib.parse.urlparse(url_link).path
            # print(urllib.parse.urlparse(url_link))
            
            links.append((host, path))
    
    for info in links:
        host, url = info
        insert_link(host, url)
    # print(final_site)

def get_next():


    sql = """
    SELECT mainsite.base_url, url.url, url.id
    FROM (
        SELECT id, base_url, search
        FROM mainsite
        WHERE mainsite.search = true 
        ORDER BY random()
        LIMIT 1
    ) AS mainsite
    LEFT JOIN url ON url.mainsite_id = mainsite.id
    WHERE mainsite.search = true AND url.id NOT IN (
        SELECT url_id FROM visit
    )
    ORDER BY random()
    LIMIT 50;
    """

    try:
        cur = conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        cur.close()
        return rows
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

    return None

def main():
    global conn
    conn = psycopg2.connect(conn_string)
    while True:
        ret = get_next();
        # print(ret)
        if ret != None:
            for i in ret:
                domain, url, url_id = i
                # print(domain, url)
                get_and_insert_site (domain, url, url_id)
        else:
            sleep(5)

def signal_handler(sig, frame):
    print('You pressed Ctrl+C!')
    if conn is not None:
        conn.close()
    sys.exit(0)

if __name__ == "__main__":
    # execute only if run as a script
    signal.signal(signal.SIGINT, signal_handler)
    main()

