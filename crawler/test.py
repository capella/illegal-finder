import urllib.request 
from bs4 import BeautifulSoup
import os, psycopg2, hashlib
from urllib.parse import urljoin
import urllib.parse
import re
import signal
import sys

from time import sleep

conn_string = "host='db' dbname='postgres' user='postgres' password='"+os.environ['POSTGRES_PASSWORD']+"'"
conn = None


def site_request (url):
    headers = {
        "Connection": "keep-alive",
        "Cache-Control": "max-age=0",
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.9"
    }
    try:
        request = urllib.request.Request(url, headers=headers)
        req = urllib.request.urlopen(request)
        encoding=req.headers['content-type'].split('charset=')[-1]
        encoding=encoding.split('Charset=')[-1]
        if encoding == "text/html":
            encoding = 'utf-8'
        return req.read().decode(encoding)
    except (Exception) as e:
        print("10", e)
    return None

def get_and_insert_site (domain, url):
    site = domain+url

    if domain in url:
        site = url 

    if (".jpg" in url) or (".jpeg" in url) or ".png" in url:
        return

    final_site = "http://"+site
    html = site_request("http://"+site)
    if html == None:
        final_site = "https://"+site
        html = site_request("https://"+site)
    if html == None:
        return

    # print(html)

    soup = BeautifulSoup(html, 'html.parser')

    [soup.extract() for soup in soup(['style', 'script', '[document]', 'head', 'title'])]
    content = " ".join(soup.strings)
    content = re.sub("\s\s+" , " ", content)

    links = []
    for link in soup.find_all('a'):
        url_link = urljoin(final_site, link.get('href'))

        print(url_link, link.get('href'))

        if url_link == None:
            continue

        if url_link.startswith('http'):
            # host = urllib.parse.urlparse(url_link).netloc
            # path = url_link[url_link.index(host)+len(host):]
            host = urllib.parse.urlparse(url_link).netloc
            path = urllib.parse.urlparse(url_link).path
            print(urllib.parse.urlparse(url_link))
            
            links.append((host, path))
    
    print(links)


get_and_insert_site("telecstv.com", "/")

