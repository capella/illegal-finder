from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import accuracy_score
import pickle
import psycopg2
from time import sleep

conn_string = "host='db' dbname='postgres' user='postgres' password='example'"

def get_all ():
    global conn
    """ query parts from the parts table """
    conn = psycopg2.connect(conn_string)
    data = []
    labels = []

    sql1 = """
        SELECT visit.content, url.mainsite_id FROM visit
        INNER JOIN url ON visit.url_id = url.id
    """

    sql2 = """
        SELECT illegalproduct FROM illegal_mainsite
        WHERE mainsite_id = %s
    """

    cur = conn.cursor()
    cur.execute(sql1)
    rows = cur.fetchall()
    cur.close()

    count = 0.0
    for i in rows:
        site_id = i[1]
        content = i[0]

        cur = conn.cursor()
        cur.execute(sql2, (site_id, ))
        rows_illegal = cur.fetchall()
        cur.close()

        aux = []
        for j in rows_illegal:
            aux.append(j[0])

        data.append(content)
        labels.append(aux)

    if conn is not None:
        conn.close()
    return (data, labels)

# ------------------------------

while True:
    data, aux = get_all()

    tobin = MultiLabelBinarizer()
    aux = tobin.fit_transform(aux)

    # X_train, X_test, y_train, y_test = train_test_split(data, aux, test_size=0, shuffle=True)
    X_train, y_train = data, aux


    vectorizer = TfidfVectorizer()
    X_train = vectorizer.fit_transform(X_train)
    # X_test = vectorizer.transform(X_test)

    classifiers = []
    for i in range(len(tobin.classes_)):
        print('... Processing ', tobin.classes_[i])

        train = [j[i] for j in y_train]
        # test = [j[i] for j in y_test]

        clf = RandomForestClassifier(n_estimators=1000, n_jobs=8)
        clf.fit(X_train, train)

        classifiers.append({
            "id": tobin.classes_[i],
            "c": clf
        })

        # prediction = clf.predict(X_test)
        # print('Test accuracy is {}'.format(accuracy_score(test, prediction)))

    with open('./result/result.pkl', 'wb') as f:
        pickle.dump((tobin, vectorizer, classifiers), f)

    del tobin
    del vectorizer
    del classifiers
    del X_train
    del aux
    del data
    sleep(10*60*60)
