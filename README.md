# Illegal Finder

Esse projeto tem como funalidade facilitar a classificação de páginas web baseado no testo que elas possuem. Ele foi desenvlvido para ter a capacidade de capturar o conteúdo de diversas páginas e realizar a classificação delas utilizando um o algorítmo Randon Flores.

Um ponto importante desse projeto é a sua capacidade de escalar o n;umero de crawlers e classificadores. Isso se deve ao fato de todos o projeto estar presente em containeres, os quais possuem uma fácil API de controle.

# Executando o projeto

Para conseguir iniciar o projeto é necessários ter o Docker instalado. Com ele instalado é preciso inicializar seu gerenciador de clusters, o Docker Swarm. Para isso:

```
docker swarm init
```

Note que você pode facilmente adicionar outrar máquinas ao seu cluster.

Com o cluster montado é necessário iniciar os containers. Para isso execute o seguinte comanado:

```
git clone https://gitlab.com/capella/illegal-finder
cd illegal-finder
docker-compose build

docker stack deploy --compose-file docker-compose.yml NAME
```

Note que NAME será um valor escolhido por você. 

Após esses passos, você pode acessar a página de controle na porta 3000. O login padrão é admin com a senha admin.

Na porta 8000 é criado um serviço que apresenta uma API. Essa API precisa receber uma requisição POST com o seu conteúdo no formato json como no seguinte exemplo:

```
{
   “sites”: [
      “​http://www.estadao.com.br/”,
      “​http://garotacomlocal.com.br/”,
      “​XXXXXXXXX”
   ],
   “callback”: “​http://calback.com.br/teste
}
```

Onde os sites são endereços que você quer classificar e o “callback” será a url a ser chamada após a clissificação ficar pronta. É realizado uma requisição POST para o endereço de callback com a seguinte estrutura:

```
{
   "sites": [
      {
         "url":"http://www.estadao.com.br",
         "reasons":[],
         "result":{
            "1":0.007,
            "2":0.004,
            "5":0.006,
            "6":0.002,
            "8":0.004
         },
         "restrict":false
      },
      {
         "url":"http://garotacomlocal.com.br",
         "reasons":["Se parece com sites que vendem Prostituição."],
         "result":{
            "1":0.886,
            "2":0.001,
            "5":0,
            "6":0.002,
            "8":0
         },
         "restrict":true
      }
   ],
   "categories": {
      "1":"Prostituição",
      "2":"Medicamentos Proibidos",
      "3":"Drogas Sintéticas","4":
      "Drogas Naturais",
      "5":"Produtos Tecnológicos Ilícitos",
      "6":"Cigarros",
      "7":"Esquemas de Pirâmide","8":"Armas"}
   }

```

Antes de realizar qualquer classificação é necessário entrar no painel de configuraçoes, adicionar alguns sites já conhecidos e suas categorias e montar o classificador clicando no botão "Restart Random Florest".

Versão Online: http://perigo.melvans.com/

Mais informações sobre o projeto podem ser vista em https://trello.com/b/bW6olJHT/.

### A fazer

Seria interessante testar outros modelos de aprendizagem.

### Video

[![](http://img.youtube.com/vi/OznNbxP7NSY/0.jpg)](http://www.youtube.com/watch?v=OznNbxP7NSY "Video Demonstrativo")


### Trello

https://trello.com/b/bW6olJHT/