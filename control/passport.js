var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var db = require('./db')

passport.use(new LocalStrategy(
    function(username, password, done) {
        db.query_email_pass (username, password, (err, res) => {
            if (err) { return done(err); }
            if (res.rows.length != 1)
                return done(null, false, { message: 'Incorrect username or password.' });
            return done(null, res.rows[0]);
        })
    }
));

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    var sql = 'SELECT id, email FROM users where id=$1';
    db.query(sql, [id], (err, res) => {
        if (err) return console.log(err);
        done(err, res.rows[0]);
    });
});

module.exports = passport;
