var id = null;

socket.on('connection', function () {
    id = encodeURI(socket.id);
});

socket.on('info', function (data) {
    console.log(data);


    $( "#loading" ).fadeOut(function() {
        var text = "";
        if (data.error) {
            text = data.error;
        } else {
            var siteinfo = data.sites[0];
            for (i in siteinfo.result) {
                text += data.categories[i]+": "+siteinfo.result[i];
                text += "<br>"
            }
            if (siteinfo.restrict) {
                text += "Site Restrito<br>";
                text += siteinfo.reasons.join("<br>");
            }
            else text += "Site Liberado";
        }
        $("#info").html(text);
        $("#info" ).fadeIn();
        $("#back" ).fadeIn();
    });
});

$( "#form" ).submit(function( event ) {
    var url = $("#search").val();
    if (!url.match(/^[a-zA-Z]+:\/\//))
        url = 'http://' + url;
    var senddata = {
        "sites": [
            url
        ],
        "callback": "http://"+window.location.hostname+"/callback/"+id
    }
    post(senddata)
    $( "#search-area" ).fadeOut();
    $( "#loading" ).fadeIn();

    event.preventDefault();
});

$( "#back" ).click(function( event ) {
    $("#info").fadeOut(0);
    $("#back").fadeOut(0);
    $( "#search-area").fadeIn(0);
});

function post(senddata) {
    $.ajax({
        type: "POST",
        url: "/callback",
        // The key needs to match your method's input parameter (case-sensitive).
        data: JSON.stringify(senddata),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function(data){
        },
        failure: function(errMsg) {
            $( "#loading" ).fadeOut();
            $( "#search-area" ).fadeIn();
        }
    });
}
