var express = require('express');
var router = express.Router();
var db = require('../db')
var passport = require('../passport')

var Docker = require('dockerode');
var docker = new Docker()

const paginate = require('express-paginate');

/* GET home page. */
router.get('/', function(req, res, next) {
    var sql = '\
        SELECT id, name FROM illegalproduct';

    db.query(sql, [], (err, data) => {
        if (err) return next(err);

        res.render('index', { products: data.rows });
    });
});

/* GET home page. */
router.get('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) { return res.render('login', { title: 'Express' }); }
        req.logIn(user, function(err) {
            if (err) return next(err);
            return res.redirect('/main');
        });
    })(req, res, next);
});

/* GET home page. */
router.post('/login', 
    passport.authenticate('local', {
        // failureFlash: true,
        successRedirect: '/main',
        failureRedirect: '/login'
    }),
    function(req, res) {
        res.redirect('/');
    }
);

var isAuthenticated = function (req, res, next) {
  if (req.isAuthenticated())
    return next();
  res.redirect('/login');
}

/* GET home page. */
router.get('/main', isAuthenticated, paginate.middleware(30, 100), function(req, res, next) {

    Promise.all([
        db.mainsite.findAll({ offset: req.skip, limit: req.query.limit, order: [
            ['search', 'DESC'],
            ['base_url', 'DESC']
        ]}),
        db.mainsite.count(),
        db.illegalproduct.findAll(),
        docker.listServices([]),
        docker.listTasks([]),
        db.request_status.findAll({
            limit: 15,
            include: [db.request, db.status],
            order: [['created_at', 'DESC']]
        })
    ]).then(function(values) {
        var [ data_sites, itemCount, classes, services, tasks, last] = values;
        var pageCount = Math.ceil(itemCount / req.query.limit);

        var crawler = services.filter(data => {
            return data.Spec.Labels["com.docker.stack.image"].endsWith("crawler")
        })
        crawler = crawler[0];
        var crawler_number = crawler.Spec.Mode.Replicated.Replicas;

        var classifier = services.filter(data => {
            return data.Spec.Labels["com.docker.stack.image"].endsWith("classifier")
        })
        classifier = classifier[0];
        var classifier_number = classifier.Spec.Mode.Replicated.Replicas;

        var classifier_builder = services.filter(data => {
            return data.Spec.Labels["com.docker.stack.image"].endsWith("classifier_builder")
        })
        classifier_builder = classifier_builder[0];

        tasks = tasks.filter(data => {
            if (classifier_builder != null)
                return data.ServiceID == classifier_builder.ID && data.DesiredState == "running"
            return false;
        })

        var builder_container = null;

        if (tasks.length == 1) {
            builder_container = tasks[0].Status
        }
        
        res.render('main', {
            illegal: classes,
            sites: data_sites,
            crawler_number: crawler_number,
            classifier_number: classifier_number,
            builder_container: builder_container,
            pageCount,
            itemCount,
            currentPage: req.currentPage,
            pages: paginate.getArrayPages(req)(10, pageCount, req.query.page),
            last: last
        });

    }).catch((err) => {
        return next(err);
    });
});

router.post('/main', isAuthenticated, function(req, res, next) {
  
    var url = req.body.url;
    var classes = req.body.classes;
    if (!Array.isArray(classes))
        classes = [classes];

    classes = classes.map ((val) => {
        return Number(val);
    })

    db.mainsite.findOne({
        where: {
            base_url: url.match(/:\/\/(.[^/]+)/)[1]
        }
    })
    .then(result => {
        if (result) {
            res.redirect('/main/'+result.id);
        } else {
            db.insert_mainsite (url, classes, function(err) {
                if (err) return next(err);
                res.redirect('/main');
            });
        }
    }).catch((err) => {
        return next(err);
    });
});

/* GET home page. */
router.get('/main/scale/:n/:name', isAuthenticated, function(req, res, next) {
    var name = req.params.name;
    if (name != "crawler" && name != "classifier")
        return res.redirect('/main');

    docker.listServices([], function(err, services) {

        var crawler = services.filter(data => {
            return data.Spec.Labels["com.docker.stack.image"].endsWith(name);
        })
        crawler = crawler[0];


        var service = docker.getService(crawler.ID);
        var service_spec = crawler.Spec;

        console.log(crawler)

        var opts = {
            "Name": crawler.Spec.Name,
            "version": parseInt(crawler.Version.Index),
            "TaskTemplate": crawler.Spec.TaskTemplate,
            "Mode": {
              "Replicated": {
                "Replicas": Math.max(parseInt(req.params.n), 0)
              }
            },
            "UpdateConfig": crawler.Spec.UpdateConfig,
            "EndpointSpec": crawler.Spec.EndpointSpec,
            "Labels": crawler.Spec.Labels
        };

        service.update(opts, function(err) {
            console.log(err);
            res.redirect('/main');
        });
    });
});

router.get('/main/restart/:containerid', isAuthenticated, function(req, res, next) {
    // Restart docker container
    var container = docker.getContainer(req.params.containerid);
    container.stop(function(err) {
        if (err) return next(err);
        res.redirect('/main');
    });
});

router.get('/main/delete/:id', isAuthenticated, function(req, res, next) {
    var sql1 = '\
        DELETE FROM mainsite  WHERE id = $1';

    var id = req.params.id;

    db.query(sql1, [id], (err, data) => {
        if (err) return next(err);
        res.redirect('/main');
    });
});


router.get('/main/:id/:status', isAuthenticated, function(req, res, next) {
    var sql1 = '\
        UPDATE mainsite SET search = $1 WHERE id = $2';


    var id = req.params.id;
    var status = req.params.status;
    status = status != '0'? true: false;

    db.query(sql1, [status, id], (err, data) => {
        if (err) return next(err);
        res.redirect('/main/'+id);
    });
});

router.get('/main/:id', isAuthenticated, function(req, res, next) {
    var sql1 = '\
        SELECT count(*) as c FROM visit \
        INNER JOIN url ON visit.url_id = url.id \
        WHERE url.mainsite_id=$1\
        UNION\
        SELECT count(*) as c FROM url \
        WHERE url.mainsite_id=$1 LIMIT 50';

    var sql2 = 'SELECT * FROM mainsite WHERE id=$1';

    var sql3 = '\
    SELECT illegalproduct.id, illegalproduct.name, A.id as p\
    FROM illegalproduct\
    LEFT JOIN (\
       SELECT * FROM illegal_mainsite\
       WHERE illegal_mainsite.mainsite_id = $1\
    ) as A ON illegalproduct.id = A.illegalproduct\
    ORDER BY illegalproduct.name;';


    var sql4 = '\
    SELECT * FROM url\
    WHERE url.mainsite_id=$1;';


    var id = req.params.id;
    db.query(sql1, [id], (err, data) => {
        if (err) return next(err);
        db.query(sql2, [id], (err, site) => {
            if (err) return next(err);
            db.query(sql3, [id], (err, class_illegal) => {
                if (err) return next(err);
                db.query(sql4, [id], (err, urls) => {
                    if (err) return next(err);

                    console.log(urls);
                    var text = data.rows[0]['c']+" of ";
                    if (data.rows[1] != undefined) {
                        text += data.rows[1]['c']
                    }
                    res.render('site', {
                        count: text,
                        site: site.rows[0],
                        illegal: class_illegal.rows,
                        urls: urls.rows
                    });
                });
            });
        });
    });
});

router.post('/main/:id', isAuthenticated, function(req, res, next) {
  
    var id = req.params.id;
    var classes = req.body.classes;
    if (!Array.isArray(classes))
        classes = [classes];

    classes = classes.map ((val) => {
        return Number(val);
    });

    db.update_classes (id, classes, function(err) {
        if (err) return next(err);
        res.redirect('/main/'+id);
    });

});

module.exports = router;
