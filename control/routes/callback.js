var express = require('express');
var request = require('request');
var db = require('../db')

var router = express.Router();

router.post('/', function(req, res, next) {
    request.post({
      headers: {'content-type' : 'application/json'},
      url:     'http://classifier:8000/',
      body:    JSON.stringify(req.body)
    }, function(error, response, body){
        if (error) res.json(error);
        else res.json(JSON.parse(body));
    });
});

router.post('/:id', function(req, res, next) {
    var io = require('../server').io;
    var id = decodeURI(req.params.id);

    io.to(id).emit('info', req.body);

    res.json('OK');
});

module.exports = router;
