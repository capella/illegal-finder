const {Pool} = require('pg');
var crypto = require('crypto');
const Sequelize = require('sequelize');

const sequelize = new Sequelize('postgres', 'postgres', 'example', {
    host: 'db',
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

// CREATE TABLE IF NOT EXISTS mainsite (
//     base_url SERIAL PRIMARY KEY,
//     base_url varchar(1023) NOT NULL UNIQUE,
//     search boolean NOT NULL DEFAULT true
// );
const mainsite = sequelize.define('mainsite', {
    id: {type: Sequelize.INTEGER, primaryKey: true},
    base_url: Sequelize.STRING(1023),
    search: Sequelize.BOOLEAN
}, {
    freezeTableName: true,
    timestamps: false
});

// CREATE TABLE IF NOT EXISTS illegalproduct (
//     id SERIAL PRIMARY KEY,
//     name varchar(255) NOT NULL UNIQUE
// );
const illegalproduct = sequelize.define('illegalproduct', {
    id: {type: Sequelize.INTEGER, primaryKey: true},
    name: Sequelize.STRING(255),
}, {
    freezeTableName: true,
    timestamps: false
});


// CREATE TABLE IF NOT EXISTS request (
//     id SERIAL PRIMARY KEY,
//     url varchar(1023),
//     callback varchar(1023)
// );
const request = sequelize.define('request', {
    id: {type: Sequelize.INTEGER, primaryKey: true},
    url: Sequelize.STRING(1023),
    callback: Sequelize.STRING(1023),
}, {
    freezeTableName: true,
    timestamps: false
});

// CREATE TABLE IF NOT EXISTS status (
//     id SERIAL PRIMARY KEY,
//     description varchar(1023)
// );
const status = sequelize.define('status', {
    id: {type: Sequelize.INTEGER, primaryKey: true},
    description: Sequelize.STRING(1023),
}, {
    freezeTableName: true,
    timestamps: false
});

// CREATE TABLE IF NOT EXISTS request_status (
//     id SERIAL PRIMARY KEY,
//     request_id int,
//     status_id int,
//     created_at TIMESTAMP DEFAULT now(),
//     FOREIGN KEY (request_id) REFERENCES request(id),
//     FOREIGN KEY (status_id) REFERENCES status(id)
// );
const request_status = sequelize.define('request_status', {
    id: {type: Sequelize.INTEGER, primaryKey: true},
    created_at: Sequelize.DATE
}, {
    freezeTableName: true,
    timestamps: false
});

request_status.belongsTo(status, {foreignKey: 'status_id'});
request_status.belongsTo(request, {foreignKey: 'request_id'});

const pool = new Pool({
  user: 'postgres',
  host: 'db',
  database: 'postgres',
  password: 'example'
})

pool.query_email_pass = function(username, password, callback) {

    var sha256 = crypto.createHash('sha256');
    var hash = sha256.update(password).digest('base64');
    var sql = 'SELECT id, email FROM users where email=$1 and password_hash=$2';

    pool.query(sql, [username, hash], (err, res) => {
        callback(err, res);
    })
}

pool.count_visits = function(callback) {

    var sql = 'SELECT COUNT(*) as c FROM visit';

    pool.query(sql, [], (err, res) => {
        if (err) return callback(err, res);
        callback(err, res.rows[0]['c']);
    })
}

pool.query_sites = function(callback) {

    var mainsite = 'SELECT * FROM mainsite ORDER BY search DESC, base_url ASC';
    var illegalproduct = 'SELECT * FROM illegalproduct';
    var illegal_mainsite = 'SELECT mainsite_id, illegalproduct FROM illegal_mainsite';

    pool.query(mainsite, (err, res_mainsite) => {
        if (err) callback(err, res_mainsite);
        pool.query(illegalproduct, (err, res_illegalproduct) => {
            if (err) callback(err, res_illegalproduct);

            var classes = {};
            res_illegalproduct = res_illegalproduct.rows;

            for (i in res_illegalproduct) {
                var tmp = res_illegalproduct[i];
                classes[tmp['id']] = tmp['name'];
            }

            pool.query(illegal_mainsite, (err, res_illegal_mainsite) => {
                if (err) callback(err, res_illegal_mainsite);

                res_illegal_mainsite = res_illegal_mainsite.rows;

                var join = {};
                for (i in res_illegal_mainsite) {
                    var tmp = res_illegal_mainsite[i];
                    var site_id = tmp['mainsite_id'];
                    var product_id = tmp['illegalproduct'];

                    if (!(site_id in join))
                        join[site_id] = [];

                    join[site_id].push(product_id);
                }


                res_mainsite = res_mainsite.rows.map (data => {
                    var products = [];
                    if (join[data['id']])
                        products = join[data['id']].map(product_id => {
                            return classes[product_id];
                        })
                    return {
                        search: data['search'],
                        url: data['base_url'],
                        id: data['id'],
                        products: products
                    }
                });
                callback(err, res_mainsite);
            })
        })
    })
}

pool.query_site = function(id, callback) {

    var mainsite = 'SELECT * FROM mainsite WHERE id=$1';
    var count_urls = 'SELECT COUNT(*) FROM url WHERE mainsite_id=$1';

    var illegalproduct = 'SELECT * FROM illegalproduct';
    var illegal_mainsite = 'SELECT mainsite_id, illegalproduct FROM illegal_mainsite';

    pool.query(mainsite, (err, res_mainsite) => {
        if (err) callback(err);

        pool.query(count_urls, (err, res_count_urls) => {
            if (err) callback(err);

            callback(err, res_mainsite);

        })
    })
}

pool.query_illegalproduct = function(callback) {

    var sql = 'SELECT * FROM illegalproduct ORDER BY name';

    pool.query(sql, (err, res) => {
        callback(err, res);
    })
}

pool.insert_mainsite = function(url, classes, callback) {
    var sql1 = 'INSERT INTO mainsite(base_url) VALUES($1) RETURNING id';
    var sql2 = 'INSERT INTO illegal_mainsite(mainsite_id, illegalproduct) VALUES($1, $2)';
    var sql3 = 'INSERT INTO url(url, mainsite_id) VALUES($1, $2)';

    url = url.match(/:\/\/(.[^/]+)/)[1];

    // callback
    pool.query(sql1, [url], (err, res) => {
        if (err) return callback(err, res);
        var id = res.rows[0]['id'];

        classes.forEach(class_id => {
            pool.query(sql2, [id, class_id]);
        });
        pool.query(sql3, ["/", id]);
        callback(err);
    })
}

pool.update_classes = function(site_id, classes, callback) {
    var sql1 = 'DELETE FROM illegal_mainsite WHERE mainsite_id = $1';
    var sql2 = 'INSERT INTO illegal_mainsite(mainsite_id, illegalproduct) VALUES($1, $2)';

    // callback
    pool.query(sql1, [site_id], (err, res) => {
        if (err) return callback(err, res);

        classes.forEach(class_id => {
            pool.query(sql2, [site_id, class_id]);
        });
        callback(err);
    })
}

pool.mainsite = mainsite;
pool.illegalproduct = illegalproduct;
pool.request = request;
pool.status = status;
pool.request_status = request_status;

module.exports = pool;
