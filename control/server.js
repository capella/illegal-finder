var http = require('http');
var app = require('./app');
const db = require('./db');

var ServerSingleton = (function() {
  this.io = null;
  this.configure = function(server) {
	var server = http.createServer(app);
	const io = require('socket.io')(server);

	var count = null;

	io.on('connection', (socket) => {
		socket.emit('connection');
	});

	setInterval(function(){
		db.count_visits(function(err, num) {
			if (err) return;
			if (count != null) {
				io.emit('up', num-count);
			}
			count = num;
		});
	}, 5000);

	this.io = io;
	return server;
  }

  return this;
})();

module.exports = ServerSingleton;
